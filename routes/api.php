<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
/*----------------------- * 系统登陆 * -----------------------------*/
Route::post('login', 'API\LoginController@login');

Route::get('getHomeData', 'API\HomeController@index');

/*----------------------- * 实时控制 * -----------------------------*/
Route::get('getDevicesIndex', 'API\DeviceController@index');
Route::post('pwmSwitch', 'API\DeviceController@pwmSwitch');
Route::post('createDevice', 'API\DeviceController@create');
Route::post('editDevice', 'API\DeviceController@edit');
Route::post('deleteDevice', 'API\DeviceController@delete');

/*----------------------- * 定时控制 * -----------------------------*/
Route::get('getTimes', 'API\TimeController@index');
Route::get('createTime', 'API\TimeController@create');
Route::post('deleteTime', 'API\TimeController@delete');
Route::post('updateTime', 'API\TimeController@update');
Route::get('control', 'API\TimeController@clockControl');

/*----------------------- * 计时控制 * -----------------------------*/
Route::get('getClocks', 'API\ClockController@index');
Route::get('createClock', 'API\ClockController@create');
Route::post('deleteClock', 'API\ClockController@delete');
Route::post('updateClock', 'API\ClockController@update');
Route::any('clockSwitch', 'API\ClockController@clockSwitch');
Route::get('finishControl', 'API\ClockController@finishControl');

/*----------------------- * 遥控控制 * -----------------------------*/
Route::get('getRemotesIndex', 'API\RemoteController@index');
Route::post('createRemote', 'API\RemoteController@create');
Route::post('editRemote', 'API\RemoteController@edit');
Route::post('deleteRemote', 'API\RemoteController@delete');

/*----------------------- * 开关控制 * -----------------------------*/
Route::get('getSwitchIndex', 'API\SwitchController@index');
Route::post('createSwitch', 'API\SwitchController@create');
Route::post('editSwitch', 'API\SwitchController@edit');
Route::post('deleteSwitch', 'API\SwitchController@delete');
Route::post('controlSwitch', 'API\SwitchController@control');

/*----------------------- * 感应控制 * -----------------------------*/
Route::get('getObserverIndex', 'API\ObserverController@index');
Route::get('createObserver', 'API\ObserverController@create');
Route::post('deleteObserver', 'API\ObserverController@delete');
Route::post('updateObserver', 'API\ObserverController@update');
Route::get('controlObserver', 'API\ObserverController@control');
