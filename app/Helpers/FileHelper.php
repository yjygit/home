<?php
/**
 * Created by PhpStorm.
 * User: yuga
 * Date: 18-8-22
 * Time: 上午9:11
 */

/**
 * helpers 帮助文档
 * 准备：在composer.json中的autoload的files中添加定义好的目录
 * 激活：输入命令行 composer dump-autoload
 * 注意：有可能出现无法加载库的问题，需要重新执行 composer dump-autoload
 */


/**
 * log函数
 * 说明：写入$road/log文件
 * @param $file_path
 * @param $log
 * @return bool
 */
function w_log($file_path, $log) {
    if ($file_path && $log) {
        $info = fopen($file_path . date('Y-m-d'), "a"); // 文件不存在则新增
        fwrite($info,date('Y-m-d H:i:s') . ": " . $log."\n");
        fclose($info);
        return true;
    }
    return false;
}

/**
 * 读取文件函数
 * 说明：解压路径上的文件
 * @param $file_path
 * @return bool / String
 */
function w_get($file_path) {
    // 检查文件是否存在
    if(file_exists($file_path)){
        $file_arr = file($file_path);
        return $file_arr;
    }
    return false;
}

/**
 * 读取当前文件夹下的文件名称列表
 * @param $file_path
 * @return array
 */
function w_ls($file_path) {
    $name = [];
    if(is_dir($file_path)) {
        $handler = opendir($file_path);
        while( ($filename = readdir($handler)) !== false )
        {
            //略过linux目录的名字为'.'和‘..'的文件
            if($filename != "." && $filename != "..")
            {
                $name[] = $filename;
            }
        }
        closedir($handler);
    }
    return $name;
}
