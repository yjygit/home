<?php

namespace App\Http\Controllers\API;

use App\Http\Server\Basic;
use App\Observer;
use App\On_off;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;

class ObserverController extends Controller
{
    /**
     * 首页数据
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() {

        $data = Observer::all();

        foreach ($data as $key => $info) {
            $data[$key]['weekLists'] = explode(",", $info['week']);
            $data[$key]['deviceLists'] = empty($info['device_ids']) ? [] : explode(",", $info['device_ids']);
        }

        $switches = On_off::all();

        return self::msgJson('OK', '获取成功', $data, $switches);

    }

    /**
     * 新建方法
     * @return \Illuminate\Http\JsonResponse
     */
    public function create() {

        $ob = new Observer();
        $ob->name = '新建任务';
        $ob->save();

        return self::msgJson('OK', '新建成功');
    }

    /**
     * 更新方法
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request) {

        $info = $request->all();
        $week = '';

        foreach ($info['weekDay'] as $weekDay) {$week = $week . ',' . ($weekDay ? 1 : 0);}

        $ob = Observer::find($info['id']);
        $ob->name = $info['name'];
        $ob->code = $info['code'];
        $ob->value = $info['value'];
        $ob->device_ids = sizeof($info['deviceLists']) ? implode(",", $info['deviceLists']) : '0';
        $ob->week = substr($week,1);
        $ob->begin_time = $info['begin_time'];
        $ob->end_time = $info['end_time'];
        $ob->status = $info['status'] ? 1 : 0;
        $ob->save();

        return self::msgJson('OK', '更新成功');
    }


    /**
     * 删除方法
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request) {

        $id = $request->get('id');

        if (isset($id) && $id) {
            Observer::destroy($id);
            return self::msgJson('OK', '删除成功');
        }

        return self::msgJson('FALSE', '删除失败');
    }

    /**
     * 控制方法
     * @return \Illuminate\Http\JsonResponse
     */
    public function control() {
        $week = date('w');
        $time = date('H:i');
        $observers = Observer::where([['status', 1], ['begin_time',"<=", $time], ['end_time',">=", $time]])->get()->toArray();

        foreach ($observers as $observer) {

            $value = Redis::exists($observer['code'])?(int)Redis::get($observer['code']):0;
            $eq = explode('=',$observer['value']);
            $gt = explode('>',$observer['value']);
            $lt = explode('<',$observer['value']);

            if((((sizeof($eq) == 2) && $value == $eq[1]) ||
                ((sizeof($gt) == 2) && $value >= $gt[1]) ||
                ((sizeof($lt) == 2) && $value <= $lt[1]))) {

                $weekLists = explode(",", $observer['week']);

                if ($weekLists[$week] && !empty($observer['device_ids'])) {

                    foreach (explode(",", $observer['device_ids']) as $device_id) {

                        if ($device = SwitchController::getSwitch($device_id)) {
                            $control = On_off::find($device->id);
                            $code = $control['code'];
                            Basic::sendData("CONTROL/:$code", env('TCP_SERVER_HOST'), env('TCP_SERVER_PORT'));
                            sleep(1);
                        }

                    }

                }
            }

        }

        return self::msgJson('OK', '控制成功');
    }

}
