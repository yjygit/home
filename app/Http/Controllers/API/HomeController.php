<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;

class HomeController extends Controller {

    public function index() {

        $data['card1'] = Redis::exists('HUMIDITY%HOME')?Redis::get('HUMIDITY%HOME')." %":0;
        $data['card2'] = Redis::exists('LUMIN%HOME')?Redis::get('LUMIN%HOME'):0;
        $data['card3'] = Redis::exists('TEMP%HOME')?Redis::get('TEMP%HOME')." °C":0;
        $data['card4'] = Redis::exists('PERSSURELIST%HOME')?Redis::get('PERSSURELIST%HOME'):0;
        $data['xTempData'] = [];
        $data['yTempData'] = [];
        $data['xPreData'] = [];
        $data['yPreData'] = [];
        $data['xLuminData'] = [];
        $data['xLuminData'] = [];

        $temp = Redis::exists('TEMPLIST')?Redis::hgetall('TEMPLIST'):[];
        $perssure = Redis::exists('PERSSURELIST')?Redis::hgetall('PERSSURELIST'):[];
        $lumin = Redis::exists('LUMINLIST')?Redis::hgetall('LUMINLIST'):[];
        $humidity = Redis::exists('HUMIDITYLIST')?Redis::hgetall('HUMIDITYLIST'):[];

        foreach ($temp as $k => $t) {
            $data['xTempData'][] = $k;
            $data['yTempData'][] = $t;
            $data['yTempOutData'][] = Redis::hget('TEMPOUTLIST', $k);
        }

        foreach ($perssure as $k => $t) {
            $data['xPreData'][] = $k;
            $data['yPreData'][] = $t;
        }

        foreach ($lumin as $k => $t) {
            $data['xLuminData'][] = $k;
            $data['yLuminData'][] = $t;
        }

        foreach ($humidity as $k => $t) {
            $data['xHumData'][] = $k;
            $data['yHumData'][] = $t;
        }

        return self::msgJson('OK', '获取成功', $data);

    }

}
