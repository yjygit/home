<?php

namespace App\Http\Controllers\API;

use App\Http\Server\Basic;
use App\On_off;
use App\Time;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Mockery\Exception;

class TimeController extends Controller
{
    /**
     * 初始化
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() {

        $data = Time::all();

        foreach ($data as $key => $info) {
            $data[$key]['weekLists'] = explode(",", $info['week']);
            $data[$key]['deviceLists'] = empty($info['device_ids']) ? [] : explode(",", $info['device_ids']);
        }

        $switches = On_off::all();

        return self::msgJson('OK', '获取成功', $data, $switches);
    }

    /**
     * 新建方法
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function create() {

        $time = new Time();

        $time->name = '新建任务';
        $time->week = '0,0,0,0,0,0,0';
        $time->value = 85;
        $time->begin_time = "00:00";
        $time->end_time = "23:59";
        $time->status = 0;
        $time->save();

        return self::msgJson('OK', '新建成功');
    }

    /**
     * 删除方法
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request) {

        $id = $request->get('id');

        if (isset($id) && $id) {
            Time::destroy($id);
            return self::msgJson('OK', '删除成功');
        }

        return self::msgJson('FALSE', '删除失败');
    }

    /**
     * 更新方法
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request) {

        $info = $request->all();
        $week = '';

        foreach ($info['weekDay'] as $weekDay) {$week = $week . ',' . ($weekDay ? 1 : 0);}

        $time = Time::find($info['id']);
        $time->name = $info['name'];
        $time->week = substr($week,1);
        $time->value = $info['value'];
        $time->device_ids = sizeof($info['deviceLists']) ? implode(",", $info['deviceLists']) : '0';
        $time->begin_time = $info['begin_time'];
        $time->end_time = $info['end_time'];
        $time->status = $info['status'] ? 1 : 0;
        $time->save();

        return self::msgJson('OK', '更新成功');
    }

    /**
     * 定时控制方法
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public static function clockControl() {

        $week = date('w');

        $time = date('H:i');

        $weekDays = Time::where('begin_time',$time)->get();

        foreach ($weekDays as $weekDay) {

            $weekLists = explode(",", $weekDay['week']);

            if ($weekDay['status'] && $weekLists[$week] && !empty($weekDay['device_ids'])) {

                try {

                    foreach (explode(",", $weekDay['device_ids']) as $device_id) {

                        if ($device = SwitchController::getSwitch($device_id)) {

                            /* tcp 控制 */

                            $control = On_off::find($device->id);

                            $code = $control['code'];

                            Basic::sendData("CONTROL/:$code", env('TCP_SERVER_HOST'), env('TCP_SERVER_PORT'));

                            sleep(1);
                        }

                    }

                } catch (Exception $exception) {

                    Basic::errorLogs("function clockControl error: ".$exception->getMessage());

                    return self::msgJson('ERROR', '控制出错');
                }
            }
        }

        return self::msgJson('OK', '控制成功');
    }

    /**
     * 时间显示方法
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public static function showTimeControl() {

        $title = "SHOW/:SHOW001";

        $time = date('H:i');
        $date = date('Y/m/d');
        
        Basic::sendData("$title/:LINE_1", env('TCP_SERVER_HOST'), env('TCP_SERVER_PORT')); sleep(2);
        Basic::sendData("$title/:Time.   $time   ", env('TCP_SERVER_HOST'), env('TCP_SERVER_PORT')); sleep(2);
        Basic::sendData("$title/:LINE_2", env('TCP_SERVER_HOST'), env('TCP_SERVER_PORT')); sleep(2);
        Basic::sendData("$title/:Date. $date", env('TCP_SERVER_HOST'), env('TCP_SERVER_PORT'));

        return self::msgJson('OK', '下发成功');
    }
}
