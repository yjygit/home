<?php

namespace App\Http\Controllers\API;

use App\Device;
use App\Http\Server\Basic;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Exception;

class DeviceController extends Controller
{
    /**
     * 首页获取数据
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index(Request $request) {

		if (!empty($request->get('type'))) {

			$devices = Device::where('type', $request->get('type'))->get();
			$control_devices = Device::where('type', Device::CONTROL)->get();

			foreach ($devices as $key => $device) {
				$devices[$key]['deviceLists'] = empty($device['device_ids']) ? [] : explode(",", $device['device_ids']);
			}

			return self::msgJson('OK', '获取成功', $devices, $control_devices);

		}

		return self::msgJson('FAIL', '获取失败');
    }

    /**
     * 新建
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request) {
		$info = $request->all();

		if (sizeof($info)) {
			Device::create($info);
			return self::msgJson('OK', '新建成功');
		}

		return self::msgJson('FAIL', '新建失败');
    }

    /**
     * 修改
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Request $request) {
		$info = $request->all();

		if (!($device = Device::find($info['id']))) {
			return self::msgJson('FAIL', '找不到该设备');
		}

		$device->name = isset($info['name'])?$info['name']:"控制设备".rand(0,1024);
		$device->code = $info['code'];
		$device->scale = $info['scale'];
		$device->value = $info['value'];
		$device->status = $info['status'];
		$device->device_ids = isset($info['deviceLists'])?implode(',',$info['deviceLists']):'0';
		$device->begin_time = isset($info['begin_time'])?$info['begin_time']:'';
		$device->end_time = isset($info['end_time'])?$info['end_time']:'';
		$device->delay_time = isset($info['delay_time'])?$info['delay_time']:'';
		$device->description = $info['description'];
		$device->save();

		return self::msgJson('OK', '更新成功');
    }

    /**
     * 删除
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request) {

		$id = $request->get('id');

		if (!empty($id)) {
			Device::destroy($id);
		}

		return self::msgJson('OK', '删除成功');
    }

    /**
     * 获取连接id
     *
     * @param $device_id
     * @return mixed
     */
    public static function getDevice($device_id) {
		return Device::find($device_id);
    }

    /**
     * 控制
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function pwmSwitch(Request $request) {

		$info = $request->all();

		try {
			$device = Device::find($info['id']);

			if ($data = Basic::sendData("CONTROL/:".$device['code'].$info['value'], env('TCP_SERVER_HOST'), env('TCP_SERVER_PORT'))) {

				if ($info['value'] == 'OPEN' || $info['value'] == 'CLOSE') {
					$device->status = ($info['value'] == 'OPEN') ? Device::OPEN : Device::CLOSE;
					$device->save(); // 更新开关状态
				}

				return self::msgJson('OK', '控制成功');
			}

		} catch (Exception $exception) {
			/* 这里写错误日志 */
			Basic::errorLogs("function pwmSwitch error: ".$exception->getMessage());
		}

        return self::msgJson('FAIL', '控制失败');
    }
}
