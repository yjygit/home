<?php

namespace App\Http\Controllers\API;

use App\Clock;
use App\On_off;
use App\Remote;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redis;

class RemoteController extends Controller
{
    /**
     * 首页获取数据
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() {

        $remotes = Remote::all();

        $devices = On_off::all();

        $clocks = Clock::all();

        foreach ($remotes as $key => $remote) {
            $remotes[$key]['deviceLists'] = empty($remote['device_ids'])?[]:explode(",", $remote['device_ids']);
            $remotes[$key]['clockLists'] = empty($remote['clock_ids'])?[]:explode(",", $remote['clock_ids']);
        }

        return self::msgJson('OK', '获取成功', $remotes, ['devices'=>$devices, 'clocks'=>$clocks]);
    }

    /**
     * 新建
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(Request $request) {
        $info = $request->all();
        if (sizeof($info)) {
            Remote::create($info);
            return self::msgJson('OK', '新建成功');
        }
        return self::msgJson('FAIL', '新建失败');
    }

    /**
     * 修改
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function edit(Request $request) {

        $info = $request->all();

        if (!($remote = Remote::find($info['id']))) {
            return self::msgJson('FAIL', '找不到该设备');
        }

        Redis::del($remote['code'].'%'.$remote['control']);

        /* 维护 redis */
        if ($info['status']) {

            $data = [];

            if (isset($info['deviceLists'])) {
                $data = array_merge($data, On_off::whereIn('id', $info['deviceLists'])->pluck("code")->toArray());
            }

            if (isset($info['clockLists']) && sizeof($info['clockLists'])) {
                $data = array_merge($data, $info['clockLists']);
            }

            Redis::set($info['code'].'%'.$info['control'], implode(',',$data));

        }

        $remote->name = isset($info['name'])?$info['name']:"遥控设备".rand(0,1024);
        $remote->code = $info['code'];
        $remote->control = $info['control'];
        $remote->device_ids = isset($info['deviceLists'])?implode(',',$info['deviceLists']):'0';
        $remote->clock_ids = isset($info['clockLists'])?implode(',',$info['clockLists']):'0';
        $remote->status = $info['status'];
        $remote->save();

        return self::msgJson('OK', '更新成功');
    }

    /**
     * 删除
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request) {

        $id = $request->get('id');

        if ($remote = Remote::find($id)) {

            /* 同时删除 redis 记录 */
            Redis::del($remote['code'].'%'.$remote['control']);

            Remote::destroy($id);

            return self::msgJson('OK', '删除成功');

        }
        return self::msgJson('FALSE', '删除失败');
    }

}
