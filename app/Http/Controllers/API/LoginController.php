<?php namespace App\Http\Controllers\API;
/**
 * Created by PhpStorm.
 * User: yuga
 * Date: 18-11-8
 * Time: 下午3:14
 */

use App\Http\Controllers\Controller;

class LoginController extends Controller
{

    /**
     * 登陆接口
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        return response()->json(['msg' => 'ok', 'code' => 200, 'user' => 'admin']);
    }
}