<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * obj to array
     * @param $objs
     * @return array
     */
    public static function objToArr($objs) {
        $arr = [];
        foreach ($objs as $key => $obj) {
            $arr[$key] = $obj;
        }
        return $arr;
    }

    /**
     * response
     * @param null $code
     * @param null $msg
     * @param null $data
     * @param null $info
     * @return \Illuminate\Http\JsonResponse
     */
    public static function msgJson($code=null, $msg=null, $data=null, $info=null) {
        return response()->json(['code' =>$code, 'msg' => $msg, 'data' => $data, 'info'=>$info]);
    }

    /**
     * get Tencent token and ticket
     * @return array
     */
    public static function getMvsToken() {
        return [
            'token' => Redis::get('token_' . $_COOKIE['mvs_open_login']),
            'iotim_ticket' => Redis::get('ticket_' . $_COOKIE['mvs_open_login'])
        ];
    }

    public static function getObjData($obj, $fromObj) {
        $newObj = [];
        foreach ($obj as $key => $value) {
            $newObj->$key = $fromObj->$key;
        }
        return $newObj;
    }
}
