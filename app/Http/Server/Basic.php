<?php namespace App\Http\Server;

class Basic {

    /**
     * 下发控制指令
     *
     * AF_INET	IPv4 网络协议。TCP 和 UDP 都可使用此协议。
     * SOCK_STREAM	提供一个顺序化的、可靠的、全双工的、基于连接的字节流。支持数据传送流量控制机制。TCP 协议即基于这种流式套接字。
     * TCP 能够保障所有的数据包是按照其发送顺序而接收的。
     *      如果任意数据包在通讯时丢失，TCP 将自动重发数据包直到目标主机应答已接收。
     *      因为可靠性和性能的原因，TCP 在数据传输层使用 8bit 字节边界。
     *      因此，TCP 应用程序必须允许传送部分报文的可能。
     *
     * @param $data
     * @param $ip
     * @param $port
     * @param bool $type
     * @param int $timeout
     * @return bool
     */
    public static function sendData($data, $ip, $port, $type = false, $timeout = 5) {

        if ($socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP)) {

            $attempts = 0;

            /* 检查链接 */
            while (!($connected = @socket_connect($socket, $ip, $port)) && $attempts++ < $timeout) {
                $error = socket_last_error();
                if ($error != SOCKET_EINPROGRESS && $error != SOCKET_EALREADY) {
                    return false;
                }
                sleep(1);
            }
            if (!$connected) {
                return false;
            }

            /* 接收命令 */
            if($type && ($response = socket_read($socket, 1024))) {

            }

            /* 发送信息 */
            socket_write($socket, $data, strlen($data));

            /* 关闭链接 */
            socket_close($socket);

        }

        return true;
    }

    /**
     * 调试日志
     * @param $log
     */
    public static function debugLogs($log) {
        w_log(env('LOGS_PATH').'Debug', $log);
    }

    /**
     * 错误日志
     * @param $log
     */
    public static function errorLogs($log) {
        w_log(env('LOGS_PATH').'Error', $log);
    }

    /**
     * 运行日志
     * @param $log
     */
    public static function infoLogs($log) {
        w_log(env('LOGS_PATH').'Info', $log);
    }

    /**
     * 失败日志
     * @param $log
     */
    public static function failLogs($log) {
        w_log(env('LOGS_PATH').'Fail', $log);
    }
}