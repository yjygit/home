<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Time extends Model
{
    protected $hidden = ['create_at', 'update_at'];

    protected $fillable = [
        'id',
        'name',
        'week',
        'value',
        'device_ids',
        'status',
        'time',
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];
}