<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Remote extends Model
{
    /* 状态开 */
    const OPEN = 1;

    /* 状态关 */
    const CLOSE = 0;

    public $table = 'remotes';

    protected $hidden = [];

    public $fillable = [
        'id',
        'name',
        'code',
        'control',
        'status',
        'device_ids',
        'clock_ids',
    ];
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];
}
