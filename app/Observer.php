<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Observer extends Model
{

    /* 状态开 */
    const OPEN = 1;

    /* 状态关 */
    const CLOSE = 0;

    public $table = 'observers';

    protected $hidden = [];

    public $fillable = [
        'name',
        'code',
        'value',
        'device_ids',
        'week',
        'begin_time',
        'end_time',
        'status'
    ];

}
