<?php

namespace App\Console\Commands;

use App\Http\Controllers\API\ClockController;
use App\Http\Controllers\API\TimeController;
use Illuminate\Console\Command;

class minServer extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'minServer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        TimeController::clockControl();
        //TimeController::showTimeControl();
    }
}
