<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    /* 设备类型： 控制设备 */
    const CONTROL = 1;

    /* 设备类型： 监控设备 */
    const MONITOR = 2;

    /* 状态开 */
    const OPEN = 1;

    /* 状态关 */
    const CLOSE = 0;

    public $table = 'devices';

    protected $hidden = [];

    public $fillable = [
        'name',
        'code',
        'scale',
        'value',
        'type',
        'status',
        'week',
        'device_ids',
        'begin_time',
        'end_time',
        'delay_time',
        'remark',
    ];
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];
}
