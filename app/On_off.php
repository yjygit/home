<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class On_off extends Model
{
    public $table = 'switches';

    protected $hidden = [];

    public $fillable = [
        'id',
        'name',
        'code',
        'description',
    ];
    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];
}
