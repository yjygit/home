<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clock extends Model
{
    const NAME = "CLOCK_";

    /* 打开 */
    const OPEN = 1;

    /* 关闭 */
    const CLOSE = 0;

    protected $hidden = ['create_at', 'update_at'];

    protected $fillable = [
        'id',
        'name',
        'value',
        'device_ids',
        'time',
        'status',
    ];
}
